import 'package:flutter/material.dart';

import 'package:progress_indicator_button/progress_button.dart';

class SettingModalBottomSheetFilter extends StatefulWidget {
  @override
  _SettingModalBottomSheetFilter createState() =>
      _SettingModalBottomSheetFilter();
}

class _SettingModalBottomSheetFilter
    extends State<SettingModalBottomSheetFilter> {
  //final controller = Modular.get<HeroesController>();

  @override
  Widget build(BuildContext context) {
    return IconButton(
        icon: Image.network(
          'https://cdn2.iconfinder.com/data/icons/font-awesome/1792/filter-512.png',
          color: Colors.white,
        ),
        onPressed: () {
          _settingModalBottomSheet(context);
        });
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: const Radius.circular(15.0),
              topRight: const Radius.circular(15.0)),
        ),
        context: context,
        isScrollControlled: true,
        builder: (BuildContext context) {
          return Container(
            padding: EdgeInsets.all(20.0),
            height: MediaQuery.of(context).size.height / 5.0 * 3.4,
            child: DraggableScrollableSheet(
                initialChildSize: 1,
                minChildSize: 0.95,
                builder:
                    (BuildContext context, ScrollController scrollController) {
                  return SingleChildScrollView(
                    controller: scrollController,
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "filtro por ID".toUpperCase(),
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15.0,
                                  color: Colors.grey),
                            ),
                            Icon(
                              Icons.close,
                              size: 24.0,
                            ),
                          ],
                        ),
                        Divider(
                          height: 10.0,
                          color: Colors.black,
                        ),
                        SizedBox(
                          height: 15.0,
                        ),
                        configureRow(
                            Icons.format_list_numbered, "Número do ID"),
                        Container(
                          width: 290,
                          child: TextField(
                            cursorColor: Colors.green,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                hintText: "Digite o ID...",
                                hintStyle: TextStyle(
                                    fontSize: 15,
                                    color: Colors.grey.shade300,
                                    fontStyle: FontStyle.italic)),
                          ),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Divider(
                          height: 10.0,
                          color: Colors.black,
                        ),
                        SizedBox(
                          height: 30.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              width: 220,
                              height: 40,
                              child: ProgressButton(
                                color: Colors.green,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15)),
                                strokeWidth: 2,
                                child: Text(
                                  "Filtrar resultado",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  );
                }),
          );
        });
  }

  Widget configureRow(IconData icon, String text) {
    return Container(
      padding: EdgeInsets.only(left: 16.0),
      child: Row(
        children: <Widget>[
          Icon(
            icon,
            size: 24.0,
            color: Colors.green,
          ),
          SizedBox(
            width: 15.0,
          ),
          Text(
            text,
            style: TextStyle(
                fontWeight: FontWeight.bold, color: Colors.grey.shade700),
          ),
        ],
      ),
    );
  }
}
