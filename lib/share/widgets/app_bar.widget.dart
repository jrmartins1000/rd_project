import 'package:flutter/material.dart';
import 'package:rd_project/share/components/filter_componets.dart';

Widget buildAppBarRequests(BuildContext context) {
  return AppBar(
    title: Text(
      'A Raia Drogasil',
      style: TextStyle(color: Colors.white),
    ),
    centerTitle: true,
    iconTheme: IconThemeData(color: Colors.white),
    actions: <Widget>[
      SettingModalBottomSheetFilter(),
    ],
    backgroundColor: Colors.green,
  );
}
