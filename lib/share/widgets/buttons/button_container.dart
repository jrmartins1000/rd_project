import 'package:flutter/material.dart';

class ButtonContainer extends StatelessWidget {
  final Widget itens;
  final double tamanho;
  final double padding;
  final double radius;

  ButtonContainer({
    @required this.itens,
    @required this.tamanho,
    @required this.padding,
    @required this.radius,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        //BackButton(),
        SizedBox(
          height: 10,
        ),
        buildContainer(itens, tamanho, padding, radius),
      ],
    );
  }
}

Widget buildContainer(
    Widget itens, double altura, double padding, double radius) {
  return Container(
    width: double.infinity,
    padding: EdgeInsets.all(padding),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(radius),
      color: Colors.white,
      boxShadow: [
        new BoxShadow(
          color: Colors.black12,
          offset: new Offset(1, 2.0),
          blurRadius: 5,
          spreadRadius: 1,
        ),
      ],
    ),
    child: itens,
  );
}
