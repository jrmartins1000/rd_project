class Constants{
  static const String BASE_URL = 'https://akabab.github.io/superhero-api/api';
  static const String HERO_LIST = '/all.json';
}