import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:rd_project/app/modules/heroes/heroes_module.dart';
import 'package:rd_project/app/modules/heroesDetail/heroes_detail_module.dart';
import 'package:rd_project/share/utilities/constants.dart';

import 'app_widget.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((i) => Dio(
            BaseOptions(baseUrl: Constants.BASE_URL, connectTimeout: 5000))),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, module: HeroesModule()),
        ModularRouter('/heroesdetail', module: HeroesDetailModule()),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
