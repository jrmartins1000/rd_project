// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'heroes_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HeroesController on _HeroesController, Store {
  final _$heroesListAtom = Atom(name: '_HeroesController.heroesList');

  @override
  List<HeroesModel> get heroesList {
    _$heroesListAtom.reportRead();
    return super.heroesList;
  }

  @override
  set heroesList(List<HeroesModel> value) {
    _$heroesListAtom.reportWrite(value, super.heroesList, () {
      super.heroesList = value;
    });
  }

  final _$getHeroesListAsyncAction =
      AsyncAction('_HeroesController.getHeroesList');

  @override
  Future<List<HeroesModel>> getHeroesList() {
    return _$getHeroesListAsyncAction.run(() => super.getHeroesList());
  }

  @override
  String toString() {
    return '''
heroesList: ${heroesList}
    ''';
  }
}
