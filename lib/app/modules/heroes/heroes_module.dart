import 'heroes_controller.dart';
import 'repositories/heroes_repository.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'heroes_page.dart';

class HeroesModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => HeroesController()),
        Bind((i) => HeroesRepository()),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, child: (_, args) => HeroesPage()),
      ];

  static Inject get to => Inject<HeroesModule>.of();
}
