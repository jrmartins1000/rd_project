import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:rd_project/app/modules/heroes/repositories/heroes_repository.dart';
import 'package:rd_project/share/models/heroes_model.dart';

part 'heroes_controller.g.dart';

@Injectable()
class HeroesController = _HeroesController with _$HeroesController;

abstract class _HeroesController with Store {
  HeroesRepository _repository;

  _HeroesController() {
    _repository = HeroesRepository();
  }

  @observable
  List<HeroesModel> heroesList;

  @action
  Future<List<HeroesModel>> getHeroesList() async {
    try {
      heroesList = await _repository.getHeroesList();
    } catch (e) {
      return e;
    }
    return heroesList;
  }
}
