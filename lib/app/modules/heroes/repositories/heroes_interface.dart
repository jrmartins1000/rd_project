import 'package:rd_project/share/models/heroes_model.dart';

abstract class IHeroes{
  Future<List<HeroesModel>> getHeroesList();
}