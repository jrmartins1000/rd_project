import 'dart:convert';

import 'package:flutter_modular/flutter_modular.dart';
import 'package:rd_project/app/modules/heroes/repositories/heroes_interface.dart';
import 'package:rd_project/share/models/heroes_model.dart';
import 'package:rd_project/share/utilities/constants.dart';
import 'package:http/http.dart' as http;

@Injectable()
class HeroesRepository extends IHeroes {
  @override
  Future<List<HeroesModel>> getHeroesList() async {
    final response =
        await http.get('${Constants.BASE_URL}${Constants.HERO_LIST}');
    if (response.statusCode == 200) {
      List<HeroesModel> typeModel = List<HeroesModel>();
      var decode = jsonDecode(response.body);
      decode.forEach((item) => typeModel.add(HeroesModel.fromJson(item)));
      return typeModel;
    } else {
      throw Exception("Erro");
    }
  }
}
