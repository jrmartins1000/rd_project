import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:rd_project/share/models/heroes_model.dart';
import 'package:rd_project/share/widgets/app_bar.widget.dart';
import 'heroes_controller.dart';

class HeroesPage extends StatefulWidget {
  final String title;

  const HeroesPage({Key key, this.title = "Heroes"}) : super(key: key);

  @override
  _HeroesPageState createState() => _HeroesPageState();
}

class _HeroesPageState extends ModularState<HeroesPage, HeroesController> {
  HeroesController _heroesController;

  @override
  void initState() {
    super.initState();

    _heroesController = HeroesController();
  }

  List<HeroesModel> listModel;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBarRequests(context),
      body: Column(
        children: <Widget>[
          Flexible(
            child: FutureBuilder<List<HeroesModel>>(
              initialData: List(),
              future: _heroesController.getHeroesList(),
              builder: (context, snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                    break;
                  case ConnectionState.waiting:
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          CircularProgressIndicator(),
                          Text("Loading"),
                        ],
                      ),
                    );
                    break;
                  case ConnectionState.active:
                    break;
                  case ConnectionState.done:
                    listModel = snapshot.data.toList();
                    if (listModel.length != 0) {
                      return ListView.builder(
                        itemBuilder: (context, index) {
                          final model = listModel[index];
                          return GestureDetector(
                            child: _requestItem(model, index),
                            onTap: () => Modular.to.pushNamed(
                                '/heroesdetail/' , arguments: model),
                          );
                        },
                        itemCount: listModel.length,
                      );
                    } else {
                      return Center(child: Text("Você não tem nenhuma Lista."));
                    }
                    break;
                }
                return Text("Error");
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _requestItem(HeroesModel model, int index) {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
      //height: 300,
      width: double.infinity,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 20,
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(6.0),
              child: Column(
                children: <Widget>[
                  topCard(model, index),
                  Container(
                    width: 300,
                    child: Divider(
                      color: Colors.black,
                      height: 15,
                    ),
                  ),
                  middleCard(model, index),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget topCard(HeroesModel model, int index) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 10, top: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          CircleAvatar(
            radius: 26,
            backgroundImage: NetworkImage(model.images.md.toString()),
            onBackgroundImageError: (exception, stackTrace) =>
                Icon(Icons.error, color: Colors.red),
          ),
          Container(
            width: 250,
            padding: const EdgeInsets.only(left: 1.0, top: 0.0),
            child: Text(
              model.name,
              textAlign: TextAlign.start,
              style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }

  Widget middleCard(HeroesModel model, int index) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 10,
          ),
          Row(
            children: <Widget>[
              Container(
                height: 8.0,
                width: 8.0,
                decoration: BoxDecoration(
                  color: Colors.grey.shade700,
                  shape: BoxShape.circle,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text(model.id.toString()),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            width: 255,
            child: Text(
              model.biography.fullName,
              textAlign: TextAlign.start,
              style: TextStyle(fontSize: 14),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildAppBar() {
    return AppBar(
      title: Text(
        "A Droga Raia".toUpperCase(),
        style: TextStyle(color: Colors.white, fontSize: 18),
      ),
      iconTheme: IconThemeData(color: Colors.white),
      centerTitle: true,
      backgroundColor: Colors.green,
    );
  }
}
