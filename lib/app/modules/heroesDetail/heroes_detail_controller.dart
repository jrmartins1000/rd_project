import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'heroes_detail_controller.g.dart';

@Injectable()
class HeroesDetailController = _HeroesDetailControllerBase
    with _$HeroesDetailController;

abstract class _HeroesDetailControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
