import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:rd_project/share/models/heroes_model.dart';
import 'package:rd_project/share/widgets/buttons/button_container.dart';
import 'heroes_detail_controller.dart';

class HeroesDetailPage extends StatefulWidget {
  final HeroesModel heroesModel;

  HeroesDetailPage({this.heroesModel});

  @override
  _HeroesDetailPageState createState() => _HeroesDetailPageState();
}

class _HeroesDetailPageState
    extends ModularState<HeroesDetailPage, HeroesDetailController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detalhes".toUpperCase()),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          color: Color(0xFFF5F5F5),
          padding: EdgeInsets.only(top: 8.0, left: 20, right: 20, bottom: 20),
          child: _itensColum(),
        ),
      ),
    );
  }

  Widget _itensColum() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        ButtonContainer(
            itens: itensContainer(), tamanho: 465, padding: 18, radius: 10),
        SizedBox(
          height: 20,
        ),
        SizedBox(
          height: 5,
        ),
      ],
    );
  }

  Widget itensContainer() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        // topCard(model),

        Container(
          padding: EdgeInsets.only(
            top: 10,
            left: 80,
            right: 0,
          ),
          child: middlePerson(),
        ),
        SizedBox(
          height: 15,
        ),
        Text(
          "Nome Completo: ${widget.heroesModel.biography.fullName.isEmpty ? "${widget.heroesModel.name}" : "${widget.heroesModel.biography.fullName}"}",
          style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
          textAlign: TextAlign.start,
        ),

        SizedBox(
          height: 20,
        ),
        Text(
          "Gênero: ${widget.heroesModel.appearance.gender}",
          style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
          textAlign: TextAlign.start,
        ),
        SizedBox(
          height: 20,
        ),

        Text(
          "Local de Nascimento: ${widget.heroesModel.biography.placeOfBirth}",
          style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
          textAlign: TextAlign.start,
        ),
        SizedBox(
          height: 20,
        ),

        Text(
          "Editora: ${widget.heroesModel.biography.publisher}",
          style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
          textAlign: TextAlign.start,
        ),

        // middleTreeItens(model),
        SizedBox(
          height: 20,
        ),

        Divider(
          color: Colors.black,
          height: 15,
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          "Fonte: Super Hero API",
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13.0),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }

  Widget middlePerson() {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            CircleAvatar(
              radius: 80,
              backgroundImage: NetworkImage(widget.heroesModel.images.lg),
            ),
            SizedBox(
              width: 10,
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }
}
