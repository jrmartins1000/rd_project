
import 'repositories/heroes_detail_repository.dart';
import 'heroes_detail_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'heroes_detail_page.dart';

class HeroesDetailModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => HeroesDetailController()),
        Bind((i) => HeroesDetailRepository()),
      ];

  @override
  List<ModularRouter> get routers => [
    ModularRouter('/heroesdetail',
        child: (_, args) => HeroesDetailPage(heroesModel: args.data ),
        transition: TransitionType.fadeIn),
  ];


  static Inject get to => Inject<HeroesDetailModule>.of();
}
